package org.teamsteamrobotics.lib.math;

public class SteamMath {

    public static final double kEpsilon = 1e-6;

    public static int factorial(int n) { return (n > 1) ? n * factorial(n - 1) : 1; }

    public static double clamp(double min, double max, double n) { return Math.max(min, Math.min(max, n)); }

    public static boolean epsilonEquals(double a, double b) { return Math.abs(a - b) < kEpsilon; }
}
