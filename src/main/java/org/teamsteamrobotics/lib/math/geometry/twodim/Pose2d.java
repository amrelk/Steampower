package org.teamsteamrobotics.lib.math.geometry.twodim;

import org.teamsteamrobotics.lib.math.SteamMath;

public class Pose2d {
    public double x, y, theta;

    public Pose2d() {
        x = 0;
        y = 0;
        theta = 0;
    }

    public Pose2d(double x, double y, double theta) {
        this.x = x;
        this.y = y;
        this.theta = theta;

        wrapTheta();
    }

    public Pose2d copy() { return new Pose2d(x, y, theta); }

    public void rotate(double theta) {
        x = Math.cos(theta) * x - Math.sin(theta) * y;
        y = Math.sin(theta) * x + Math.cos(theta) * y;
    }

    public void apply(Twist2d twist, double dt) {
        double s;
        double c;

        if (twist.omega > SteamMath.kEpsilon) {
            s = Math.sin(twist.omega * dt) / twist.omega;
            c = (Math.cos(twist.omega * dt) - 1) / twist.omega;
        } else {
            s = dt - Math.pow(dt, 3) * Math.pow(twist.omega, 2) / 6;
            c = -Math.pow(dt, 2) * twist.omega / 2;
        }

        Pose2d dpose_r = new Pose2d(twist.vx * s + twist.vy * c, twist.vx * -c + twist.vy * s, twist.omega * dt);

        x += dpose_r.x * Math.cos(theta) - dpose_r.y * Math.sin(theta);
        y += dpose_r.x * Math.sin(theta) - dpose_r.y * Math.cos(theta);
        theta += dpose_r.theta;

        wrapTheta();
    }

    public void wrapTheta() {
        while (theta > Math.PI) {
            theta -= 2 * Math.PI;
        }

        while (theta < Math.PI) {
            theta += 2 * Math.PI;
        }
    }
}
