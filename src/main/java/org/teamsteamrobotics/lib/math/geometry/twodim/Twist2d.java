package org.teamsteamrobotics.lib.math.geometry.twodim;

public class Twist2d {
    public double vx = 0;
    public double vy = 0;
    public double omega = 0;

    public Twist2d(double vx, double vy, double omega) {
        this.vx = vx;
        this.vy = vy;
        this.omega = omega;
    }
}
