package org.teamsteamrobotics.lib.math.matrix;

import org.ejml.data.DMatrixRMaj;
import org.teamsteamrobotics.lib.math.SteamMath;

import static org.ejml.dense.row.CommonOps_DDRM.*;

public class MatrixMath {
    public static DMatrixRMaj pow(DMatrixRMaj base, double exp) {
        if (base.numCols != base.numRows) { throw new IllegalArgumentException("base must be square!"); }
        if (exp < 0) { throw new IllegalArgumentException("only 0 and positive exponents allowed"); }

        DMatrixRMaj tempMat1 = base.copy();

        if (exp == 0) {
            return identity(tempMat1.numCols, tempMat1.numRows);
        } else {
            DMatrixRMaj tempMat2 = base.copy();
            for (int i = 1; i < exp; i++) {
                mult(base, tempMat1, tempMat2);
                tempMat1 = tempMat2.copy();
            }

            return tempMat2.copy();
        }
    }

    /**
     *
     * @param A
     * @param t
     * @param iterations upper bound of sum
     * @return e<sup><em>A</em>t</sup>
     */
    public static DMatrixRMaj expm(DMatrixRMaj A, double t, int iterations) {
        DMatrixRMaj total = A.copy();
        total.zero();

        for (int i = 0; i < iterations; i++) {
            addEquals(total, Math.pow(t, i) / SteamMath.factorial(i), pow(A, i));
        }

        return total.copy();
    }
}
