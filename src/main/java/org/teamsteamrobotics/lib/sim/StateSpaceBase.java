package org.teamsteamrobotics.lib.sim;

import org.ejml.data.DMatrixRMaj;
import org.teamsteamrobotics.lib.math.matrix.MatrixMath;

import static org.ejml.dense.row.CommonOps_DDRM.*;

public class StateSpaceBase implements StateSpaceModel {
    protected DMatrixRMaj Ac;
    protected DMatrixRMaj Bc;
    protected DMatrixRMaj Ad;
    protected DMatrixRMaj Bd;
    protected DMatrixRMaj C;
    protected DMatrixRMaj D;

    protected DMatrixRMaj x;
    protected DMatrixRMaj u;
    protected DMatrixRMaj y;

    protected double dt;

    public StateSpaceBase(DMatrixRMaj Ac, DMatrixRMaj Bc, DMatrixRMaj C, DMatrixRMaj D, DMatrixRMaj x, DMatrixRMaj u,
                          DMatrixRMaj y) {
        this.Ac = Ac.copy();
        this.Bc = Bc.copy();
        this.C = C.copy();
        this.D = D.copy();

        this.x = x.copy();
        this.u = u.copy();
        this.y = y.copy();
    }

    public void discretize(double dt) {
        DMatrixRMaj tmp1 = new DMatrixRMaj(1);
        DMatrixRMaj tmp2 = new DMatrixRMaj(1);
        DMatrixRMaj tmp3 = new DMatrixRMaj(1);

        Ad = MatrixMath.expm(Ac, dt, 10);

        invert(Ac, tmp1);                                     // tmp1 = Ac^-1
        subtract(Ad, identity(Ad.numRows, Ad.numCols), tmp2); // tmp2 = Ad - I
        mult(tmp1, tmp2, tmp3);                               // tmp3 = (Ac^-1) * (Ad - I)
        mult(tmp3, Bc, tmp1);                                 // tmp1 = (Ac^-1) * (Ad - I) * Bc

        Bd = tmp1.copy();

        this.dt = dt;
    }

    public void step() {
        if (dt == 0) {
            throw new IllegalStateException("model not discretized!");
        }

        DMatrixRMaj tmp1 = x.copy();

        mult(Ad, tmp1, x);
        multAdd(Bd, u, x);

        mult(C, x, y);
        multAdd(D, u, y);
    }

    public double[] x() { return x.getData(); }

    public double[] y() { return y.getData(); }

    public void setU(double[] input) { u.data = input; }
}
