package org.teamsteamrobotics.lib.sim;

public interface StateSpaceModel {
    void discretize(double dt);

    void step();

    double[] x();

    double[] y();

    void setU(double[] input);
}
