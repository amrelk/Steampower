package org.teamsteamrobotics.lib.sim.drivetrain;

import org.teamsteamrobotics.lib.sim.StateSpaceModel;

public interface DrivetrainModel extends StateSpaceModel {
    void discretize(double dt);

    void step();

    void setInput(double left, double right);

    double getForwardVelocity();
    double getTurnVelocity();
}
