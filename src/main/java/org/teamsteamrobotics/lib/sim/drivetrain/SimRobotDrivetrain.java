package org.teamsteamrobotics.lib.sim.drivetrain;

import org.teamsteamrobotics.lib.math.geometry.twodim.Pose2d;
import org.teamsteamrobotics.lib.math.geometry.twodim.Twist2d;
import org.teamsteamrobotics.lib.sim.drivetrain.tank.TankDriveModel;
import org.teamsteamrobotics.lib.sim.motor.DcBrushedMotor;

public class SimRobotDrivetrain {
    DrivetrainModel model;

    private Pose2d pose;
    private double t = 0;

    public final double dt = 0.01;

    public SimRobotDrivetrain(DrivetrainModel model) {
        this.model = model;
        this.model.discretize(dt);

        pose = new Pose2d(0, 0, Math.PI / 2);
    }

    public void setInput(double left, double right) { model.setInput(left, right); }

    public void step() {
        t += dt;
        model.step();
        pose.apply(new Twist2d(model.getForwardVelocity(), 0, model.getTurnVelocity()), dt);
    }

    public Pose2d pose() { return pose.copy(); }

    public double t() { return t; }
}
