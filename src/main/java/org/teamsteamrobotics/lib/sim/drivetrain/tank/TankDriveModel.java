package org.teamsteamrobotics.lib.sim.drivetrain.tank;

import org.ejml.data.DMatrixRMaj;
import org.teamsteamrobotics.lib.math.SteamMath;
import org.teamsteamrobotics.lib.sim.StateSpaceBase;
import org.teamsteamrobotics.lib.sim.drivetrain.DrivetrainModel;
import org.teamsteamrobotics.lib.sim.motor.DcBrushedMotor;

import static org.ejml.dense.row.CommonOps_DDRM.mult;
import static org.ejml.dense.row.CommonOps_DDRM.multAdd;

public class TankDriveModel extends StateSpaceBase implements DrivetrainModel {

    private final double minVoltage = -12;
    private final double maxVoltage = 12;

    /**
     *
     * @param motor {@link DcBrushedMotor} that defines the drivetrain motors
     * @param motorAmount number of motors per side
     * @param m mass of robot in kg
     * @param r radius of wheels in meters
     * @param rb radius of wheelbase in meters
     * @param J moment of inertia of robot in kg-m^2
     * @param Gl gear ratio of left side
     * @param Gr gear ratio of right side
     */
    public TankDriveModel(DcBrushedMotor motor, int motorAmount, double m, double r, double rb, double J, double Gl, double Gr) {
        super(new DMatrixRMaj(2, 2), new DMatrixRMaj(2, 2), new DMatrixRMaj(2, 2), new DMatrixRMaj(2, 2),
              new DMatrixRMaj(2, 1), new DMatrixRMaj(2, 1), new DMatrixRMaj(2, 1));

        motor = motor.gearbox(motorAmount);

        double C1 = -Math.pow(Gl, 2) * motor.Kt / (motor.Kv * motor.R * Math.pow(r, 2));
        double C2 = Gl * motor.Kt / (motor.R * r);
        double C3 = -Math.pow(Gr, 2) * motor.Kt / (motor.Kv * motor.R * Math.pow(r, 2));
        double C4 = Gr * motor.Kt / (motor.R * r);

        Ac.set(new double[][] {{(1 / m + Math.pow(rb, 2) / J) * C1, (1 / m - Math.pow(rb, 2) / J) * C3},
                               {(1 / m - Math.pow(rb, 2) / J) * C1, (1 / m + Math.pow(rb, 2) / J) * C3}});
        Bc.set(new double[][] {{(1 / m + Math.pow(rb, 2) / J) * C2, (1 / m - Math.pow(rb, 2) / J) * C4},
                               {(1 / m - Math.pow(rb, 2) / J) * C2, (1 / m + Math.pow(rb, 2) / J) * C4}});
        C.set(new double[][] {{0.5, 0.5}, {-1 / (2 * rb), 1 / (2 * rb)}});
        D.set(new double[][] {{0, 0}, {0, 0}});

        x.set(new double[][] {{0}, {0}});
        u.set(new double[][] {{0}, {0}});
        y.set(new double[][] {{0}, {0}});
    }

    public void setInput(double left, double right) { setU(new double[] {clampVoltage(left), clampVoltage(right)}); }

    private double clampVoltage(double voltage) { return SteamMath.clamp(minVoltage, maxVoltage, voltage); }

    @Override
    public double getForwardVelocity() {
        return y.get(0, 0);
    }

    @Override
    public double getTurnVelocity() {
        return y.get(1, 0);
    }
}