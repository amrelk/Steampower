package org.teamsteamrobotics.lib.sim.motor;

public class DcBrushedMotor {

    public double nominalVoltage, stallTorque, stallCurrent, freeCurrent, freeSpeed, R, Kv, Kt;

    public DcBrushedMotor(double nominalVoltage, double stallTorque, double stallCurrent, double freeCurrent,
                          double freeSpeed) {
        this.nominalVoltage = nominalVoltage;
        this.stallTorque = stallTorque;
        this.stallCurrent = stallCurrent;
        this.freeCurrent = freeCurrent;
        this.freeSpeed = freeSpeed / 60.0 * (2.0 * Math.PI); // convert from RPM to rad/s

        this.R = this.nominalVoltage / this.stallCurrent;                             // resistance of motor
        this.Kv = this.freeSpeed / (this.nominalVoltage - this.R * this.freeCurrent); // velocity constant
        this.Kt = this.stallTorque / this.stallCurrent;                               // torque constant
    }

    public DcBrushedMotor gearbox(int motorAmount) {
        return new DcBrushedMotor(this.nominalVoltage, this.stallTorque * motorAmount, this.stallCurrent,
                                  this.freeCurrent,
                                  this.freeSpeed / (2.0 * Math.PI) * 60 // convert from rad/s to RPM
        );
    }

    // some common motors
    public static final DcBrushedMotor MOTOR_CIM = new DcBrushedMotor(12.0, 2.42, 133.0, 2.7, 5310.0),
                                       MOTOR_MINI_CIM = new DcBrushedMotor(12.0, 1.41, 89.0, 3.0, 5840.0),
                                       MOTOR_BAG = new DcBrushedMotor(12.0, 0.43, 53.0, 1.8, 13180.0),
                                       MOTOR_775PRO = new DcBrushedMotor(12.0, 0.71, 134.0, 0.7, 18730.0),
                                       MOTOR_AM_RS775_125 = new DcBrushedMotor(12.0, 0.28, 18.0, 1.6, 5800.0),
                                       MOTOR_BB_RS775 = new DcBrushedMotor(12.0, 0.72, 97.0, 2.7, 13050.0),
                                       MOTOR_AM_9015 = new DcBrushedMotor(12.0, 0.36, 71.0, 3.7, 14270.0),
                                       MOTOR_BB_RS550 = new DcBrushedMotor(12.0, 0.38, 84.0, 0.4, 19000.0);
}
