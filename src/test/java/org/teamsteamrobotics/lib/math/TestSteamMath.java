package org.teamsteamrobotics.lib.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSteamMath {
    @Test
    public void testFactorial() {
        assertEquals(6, SteamMath.factorial(3));
        assertEquals(720, SteamMath.factorial(6));
    }

    @Test
    public void testClamp() {
        assertEquals(-1, SteamMath.clamp(-1, 1, -1.5));
        assertEquals(1, SteamMath.clamp(-1, 1, 1.5));
        assertEquals(0.5, SteamMath.clamp(-1, 1, 0.5));
    }
}
