package org.teamsteamrobotics.lib.math.matrix;

import org.ejml.data.DMatrixRMaj;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestMatrixMath {

    DMatrixRMaj A;

    @BeforeEach
    public void setup() {
        A = new DMatrixRMaj(new double[][] {{1, 2}, {3, 4}});
    }

    @Test
    public void testMatrixPow() {
        assertArrayEquals(new double[] {1, 0, 0, 1}, MatrixMath.pow(A, 0).data);
        assertArrayEquals(new double[] {1, 2, 3, 4}, MatrixMath.pow(A, 1).data);
        assertArrayEquals(new double[] {7, 10, 15, 22}, MatrixMath.pow(A, 2).data);
        assertArrayEquals(new double[] {37, 54, 81, 118}, MatrixMath.pow(A, 3).data);

        assertArrayEquals(new double[] {1, 2, 3, 4}, A.data); // make sure that A was not modified
    }

    @Test
    public void testMatrixExponential() {
        assertArrayEquals(new double[] {1.0596, 0.1137, 0.1706, 1.2301}, MatrixMath.expm(A, 0.05, 5).data, 0.0001);
    }
}
