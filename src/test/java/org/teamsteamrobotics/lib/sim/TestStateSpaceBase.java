package org.teamsteamrobotics.lib.sim;

import org.ejml.data.DMatrixRMaj;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestStateSpaceBase {

    StateSpaceBase model;

    @BeforeEach
    public void setup() {
        DMatrixRMaj Ac = new DMatrixRMaj(new double[][] {{1, 2}, {3, 4}});
        DMatrixRMaj Bc = new DMatrixRMaj(new double[][] {{5, 6}, {7, 8}});
        DMatrixRMaj C = new DMatrixRMaj(new double[][] {{9, 10}, {11, 12}});
        DMatrixRMaj D = new DMatrixRMaj(new double[][] {{13, 14}, {15, 16}});

        DMatrixRMaj x = new DMatrixRMaj(2, 1);
        DMatrixRMaj u = new DMatrixRMaj(2, 1);
        DMatrixRMaj y = new DMatrixRMaj(2, 1);

        model = new StateSpaceBase(Ac, Bc, C, D, x, u, y);
    }

    @Test
    public void testThatImNotStupid() {
        assertArrayEquals(new double[] {1, 2, 3, 4}, model.Ac.getData());
        assertArrayEquals(new double[] {5, 6, 7, 8}, model.Bc.getData());
        assertArrayEquals(new double[] {9, 10, 11, 12}, model.C.getData());
        assertArrayEquals(new double[] {13, 14, 15, 16}, model.D.getData());
    }

    @Test
    public void testDiscretization() {
        model.discretize(0.05);

        assertArrayEquals(new double[] {1.06, 0.1137, 0.1706, 1.23}, model.Ad.getData(), 0.001);
        assertArrayEquals(new double[] {0.2716, 0.3302, 0.4089, 0.4684}, model.Bd.getData(), 0.01);
    }

    @Test
    public void testDiscretizationCheck() {
        assertThrows(IllegalStateException.class, model::step);

        model.discretize(0.01);

        assertDoesNotThrow(model::step);
    }
}
