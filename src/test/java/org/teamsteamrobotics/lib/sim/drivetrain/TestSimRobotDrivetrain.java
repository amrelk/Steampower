package org.teamsteamrobotics.lib.sim.drivetrain;

import com.github.sh0nk.matplotlib4j.Plot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.teamsteamrobotics.lib.math.geometry.twodim.Pose2d;
import org.teamsteamrobotics.lib.sim.drivetrain.tank.TankDriveModel;
import org.teamsteamrobotics.lib.sim.motor.DcBrushedMotor;

import java.util.ArrayList;
import java.util.List;

public class TestSimRobotDrivetrain {
    SimRobotDrivetrain model;

    @BeforeEach
    public void setup() {
        model = new SimRobotDrivetrain(new TankDriveModel(DcBrushedMotor.MOTOR_CIM, 2, 52, 0.08255 / 2.0, 0.59055 / 2.0, 6.0, 10.71, 10.71));
    }

    @Test
    public void graphStuff() {
        List<Double> x = new ArrayList<>();
        List<Double> y = new ArrayList<>();

        model.setInput(12, 12);

        while (model.t() < 30) {
            model.step();

            Pose2d pose = model.pose();
            x.add(pose.x);
            y.add(pose.y);

            if (model.t() < 2)
                model.setInput(12, 8);
            else
                model.setInput(12 - model.t() / 10, 10);
        }

        Plot plt = Plot.create();
        plt.plot().add(x, y).label("position");
        plt.xlabel("position (m)");
        plt.ylabel("position (m)");
        plt.legend();
        try {
            plt.show();
        } catch (Exception e) { e.printStackTrace(); }
    }
}
