package org.teamsteamrobotics.lib.sim.drivetrain.tank;

import com.github.sh0nk.matplotlib4j.Plot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.teamsteamrobotics.lib.sim.motor.DcBrushedMotor;

import java.util.ArrayList;
import java.util.List;

public class TestTankDriveModel {
    TankDriveModel model;

    @BeforeEach
    public void setup() {
        model =
            new TankDriveModel(DcBrushedMotor.MOTOR_CIM, 2, 52, 0.08255 / 2.0, 0.59055 / 2.0, 6.0, 10.71, 10.71);
        model.discretize(0.01);
    }

    @Test
    public void graphStuff() {
        List<Double> vl = new ArrayList<>();
        List<Double> vr = new ArrayList<>();
        List<Double> v = new ArrayList<>();
        List<Double> w = new ArrayList<>();
        List<Double> lt = new ArrayList<>();

        model.setInput(11, 12);

        for (double t = 0; t < 10; t += 0.01) {
            model.step();
            lt.add(t);

            double[] x = model.x();
            vl.add(x[0]);
            vr.add(x[1]);

            double[] y = model.y();
            v.add(y[0]);
            w.add(y[1]);
        }

        Plot plt = Plot.create();
        plt.plot().add(lt, vl).label("left velocity");
        plt.plot().add(lt, vr).label("right velocity");
        plt.plot().add(lt, v).label("velocity");
        plt.plot().add(lt, w).label("rotational");
        plt.xlabel("time (s)");
        plt.ylabel("velocity (m/s)");
        plt.legend();
        try {
            plt.show();
        } catch (Exception e) { e.printStackTrace(); }
    }
}
